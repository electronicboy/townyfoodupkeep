package net.potatohed.TownyFoodUpkeep.Runnables;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.scheduler.BukkitRunnable;

import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;

import net.milkbowl.vault.permission.Permission;
import net.potatohed.TownyFoodUpkeep.TownyFoodUpkeep;

public class UpkeepAddRunnable extends BukkitRunnable
{
	TownyFoodUpkeep main;
	TownyUniverse tu;
	Boolean Force = false;
	public static Permission perms = null;
	public UpkeepAddRunnable(Plugin plugin)
	{
		main = (TownyFoodUpkeep) plugin;
		tu = ((Towny) Bukkit.getPluginManager().getPlugin("Towny")).getTownyUniverse();


		RegisteredServiceProvider<Permission> rsp = Bukkit.getServer().getServicesManager().getRegistration(Permission.class);
		perms = rsp.getProvider();

	}
	public UpkeepAddRunnable(Plugin plugin, boolean Force)
	{
		main = (TownyFoodUpkeep) plugin;
		tu = ((Towny) Bukkit.getPluginManager().getPlugin("Towny")).getTownyUniverse();
		this.Force = Force;

		RegisteredServiceProvider<Permission> rsp = Bukkit.getServer().getServicesManager().getRegistration(Permission.class);
		perms = rsp.getProvider();

	}
	@SuppressWarnings("deprecation")
	public void run() 
	{
		try
		{
			if (main.getConfig().getLong("NextRuntime") < System.currentTimeMillis() || Force )
			{
				main.getConfig().set("NextRuntime", (System.currentTimeMillis() + TimeUnit.HOURS.toMillis(main.getConfig().getInt("UpKeepHours"))));
				main.saveConfig();
				Bukkit.getLogger().info(main.ls.getLangFile().getString(this.getClass().getName()+ ".Begin"));
				synchronized(main.TownUpkeepList)
				{
					List<Town> towns = tu.getTowns();
					for (Town town : towns)
					{
						if(main.TownUpkeepList.containsKey(town))
						{
							main.TownUpkeepList.get(town).resetLAmount();
						}
						if(main.getConfig().isSet("ExemptTown." + town.getName()) && main.getConfig().getBoolean("ExemptTown." + town.getName()))
							continue;
						for (Resident res : town.getResidents())
						{
							OfflinePlayer p = Bukkit.getOfflinePlayer(res.getName());
							String Group = perms.getPrimaryGroup(main.getConfig().getString("DefaultWorldName"), p.getName());
							List<String> Upkeep = null;
							if (main.getConfig().isSet("Groups." + Group))
								Upkeep = main.getConfig().getStringList("Groups." + Group);
							else
								Upkeep = main.getConfig().getStringList("Groups.Default");
							if (Upkeep != null)
								for ( String str : Upkeep)
								{
									String[] data = str.split("/");
									Material mat = Material.getMaterial(data[0]);
									int amt = Integer.parseInt(data[1]);
									if (!main.addupkeep(mat, amt, town))
									{
										String Return = main.ls.getLangFile().getString(this.getClass().getName() + ".Fail");
										Return = Return.replace("%t", town.getFormattedName());
										Return = Return.replace("%u", amt+"");
										Return = Return.replace("%m", mat.toString());
										Bukkit.getLogger().warning(Return);
									}

								}
						}
					}
					main.saveData();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}

