package net.potatohed.TownyFoodUpkeep.Runnables;

import java.util.LinkedList;
import java.util.List;

import net.potatohed.TownyFoodUpkeep.TownyFoodUpkeep;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.TownyUniverse;

public class UpkeepEffectRunnable extends BukkitRunnable
{
	TownyFoodUpkeep main;
	TownyUniverse tu;
	List<Integer> PLevels;
	public UpkeepEffectRunnable(Plugin plugin)
	{
		main = (TownyFoodUpkeep) plugin;
		tu = ((Towny) Bukkit.getPluginManager().getPlugin("Towny")).getTownyUniverse();
		PLevels = new LinkedList<Integer>();

	}

	@SuppressWarnings("deprecation")
	public void run() 
	{
		Player onlinePlayers[] = Bukkit.getOnlinePlayers();
		for (Player p : onlinePlayers)
		{
			//Bukkit.getLogger().info("eval player:"+p.getName());
			try {
				if (p.hasPermission("TownyFoodUpkeep.Effects.Exempt") || !tu.getResident(p.getName()).hasTown())
					return;
			} catch (NotRegisteredException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try 
			{
				if (main.TownUpkeepList.get(tu.getResident(p.getName()).getTown()).isOverDue())
				{
					//Bukkit.getLogger().info("Punishing " + p.getName());
					int Pamount = main.TownUpkeepList.get(tu.getResident(p.getName()).getTown()).getOverDueAmount();
					int Plevel = (Pamount / main.getConfig().getInt("Punishment.Step")) + 1;
					if (Plevel > main.Punishments.size())
						Plevel = main.Punishments.size();
					doPunishment(Plevel, p);
				}

			}
			catch (Exception e) 
			{
				e.printStackTrace();
				//Checked before calling, should never be thrown
			}
		}

	}
	private boolean doPunishment(Integer Level , Player p)
	{
		try
		{
			//Bukkit.getLogger().info(p.getName() + Level);
			List<String> Puni = main.Punishments.get(Level.toString());
			//Bukkit.getLogger().info(Level.toString());
			for(String pun : Puni)
			{
				//Bukkit.getLogger().info(pun);
				String[] data = pun.split(",");
				PotionEffect potion = new PotionEffect(PotionEffectType.getByName(data[0]) ,Integer.parseInt(data[1]),Integer.parseInt(data[2]));
				potion.apply(p);						
			}
			return true;
		}
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
}
