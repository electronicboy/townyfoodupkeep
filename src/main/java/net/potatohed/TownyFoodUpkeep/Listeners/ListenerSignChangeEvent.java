package net.potatohed.TownyFoodUpkeep.Listeners;

import net.potatohed.TownyFoodUpkeep.TownyFoodUpkeep;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.material.Sign;
import org.bukkit.plugin.Plugin;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.TownyUniverse;

public class ListenerSignChangeEvent implements Listener
{
	TownyFoodUpkeep main;
	TownyUniverse tu;
	public ListenerSignChangeEvent(Plugin plugin, TownyUniverse tu)
	{
		main = (TownyFoodUpkeep) plugin;
		this.tu = tu;
 

	}
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onSignChangeEvent( SignChangeEvent e ) throws NotRegisteredException
	{
		if( e.getLine(0).equalsIgnoreCase("[WareHouse]"))
		{
			Resident res = tu.getResident(e.getPlayer().getName());
			if( res.hasTownRank("assistant") || res.isMayor() )
			{
				Sign sign = (Sign) e.getBlock().getState().getData();
				Block block = e.getBlock().getRelative(sign.getAttachedFace());
				if (!(block.getType() == Material.CHEST))
				{
					e.getPlayer().sendMessage(main.ls.getLangFile().getString(this.getClass().getName() + ".Error"));					
				} else if(main.addTownChest(res.getTown(), block.getLocation()))
				{
					e.setLine(1, res.getTown().getName());
					e.getPlayer().sendMessage(main.ls.getLangFile().getString(this.getClass().getName() + ".Allow"));
				}
				else
					return; //Exception Eventually
					
			}
			else
			{
				e.getPlayer().sendMessage(main.ls.getLangFile().getString(this.getClass().getName() + ".Deny"));
			}
				
		}
	}

}
