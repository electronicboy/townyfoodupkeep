package net.potatohed.TownyFoodUpkeep.Listeners;

import net.potatohed.TownyFoodUpkeep.TownyFoodUpkeep;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.Plugin;

import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.TownyUniverse;

public class ListenerBlockBreakEvent implements Listener
{
	TownyFoodUpkeep main;
	TownyUniverse tu;

	public ListenerBlockBreakEvent(Plugin plugin, TownyUniverse tu)
	{
		main = (TownyFoodUpkeep) plugin;
		this.tu = tu;
	}


	@SuppressWarnings("deprecation")
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e)
	{
		try
		{
			if(e.getBlock().getType() == Material.WALL_SIGN)
			{
				Sign sign = (Sign) e.getBlock().getState();
				if (sign.getLine(0).equalsIgnoreCase("[WareHouse]"))
				{
					Resident res = tu.getResident(e.getPlayer().getName());
					if (res.isMayor() ||res.hasTownRank("assistant") || e.getPlayer().hasPermission("TownyFoodUpkeep.Chest.Override"))
					{
						org.bukkit.material.Sign msign = (org.bukkit.material.Sign) sign.getData();
						Block block = e.getBlock().getRelative(msign.getAttachedFace());
						main.removeTownChest(block.getLocation());
						String Return = main.ls.getLangFile().getString(this.getClass().getName() + ".Warn");
						Return = Return.replace("%p", e.getPlayer().getName());
						Return = Return.replace("%x", block.getLocation().getBlockX()+"");
						Return = Return.replace("%y", block.getLocation().getBlockY()+"");
						Return = Return.replace("%z", block.getLocation().getBlockZ()+"");
						Bukkit.getServer().broadcast(Return, "TownyFoodUpkeep.Chest.Announce");
					}
					else
					{
						e.setCancelled(true);
						e.getPlayer().sendMessage(main.ls.getLangFile().getString(this.getClass().getName() + ".Deny"));
						return;
					}

				}
			}else if(e.getBlock().getType() == Material.CHEST)
			{
				if (main.isWarehouse(e.getBlock().getLocation()))
				{
					Resident res = tu.getResident(e.getPlayer().getName());
					if (res.isMayor() || res.hasTownRank("assistant") || e.getPlayer().hasPermission("TownyFoodUpkeep.Chest.Override"))
					{
						String Return = main.ls.getLangFile().getString(this.getClass().getName() + ".Warn");
						main.removeTownChest(e.getBlock().getLocation());
						Return = Return.replace("%p", e.getPlayer().getName());
						Return = Return.replace("%x", e.getBlock().getLocation().getBlockX()+"");
						Return = Return.replace("%y", e.getBlock().getLocation().getBlockY()+"");
						Return = Return.replace("%z", e.getBlock().getLocation().getBlockZ()+"");
						Bukkit.getServer().broadcast(Return, "TownyFoodUpkeep.Chest.Announce");
					}
					else
					{
						e.setCancelled(true);
						e.getPlayer().sendMessage(main.ls.getLangFile().getString(this.getClass().getName() + ".Deny"));
						return;
					}


				}
			}
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
		}
	}


}
