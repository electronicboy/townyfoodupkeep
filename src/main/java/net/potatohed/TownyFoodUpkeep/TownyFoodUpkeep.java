package net.potatohed.TownyFoodUpkeep;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;

import net.potatohed.TownyFoodUpkeep.Listeners.ListenerBlockBreakEvent;
import net.potatohed.TownyFoodUpkeep.Listeners.ListenerSignChangeEvent;
import net.potatohed.TownyFoodUpkeep.Objects.TownChest;
import net.potatohed.TownyFoodUpkeep.Objects.TownUpkeep;
import net.potatohed.TownyFoodUpkeep.Objects.Upkeep;
import net.potatohed.TownyFoodUpkeep.Runnables.UpkeepAddRunnable;
import net.potatohed.TownyFoodUpkeep.Runnables.UpkeepEffectRunnable;
import net.potatohed.TownyFoodUpkeep.Runnables.UpkeepRemoveRunnable;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.palmergames.bukkit.towny.Towny;
import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Resident;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;

public class TownyFoodUpkeep extends JavaPlugin
{
	public IdentityHashMap<Town, TownUpkeep> TownUpkeepList;
	public IdentityHashMap<Town, TownChest> TownChestList;
	public HashMap<String, List<String>> Punishments;
	private BukkitRunnable upkeepRemove;
	public LangStrings ls;
	private DataStorage ds;
	private TownyUniverse tu;
 
	public void onEnable() 
	{
		this.saveDefaultConfig();
			if ( !this.getConfig().isSet("Version"))
			this.getConfig().options().copyDefaults(true);
		ds = new DataStorage(this);
		ls = new LangStrings(this, this.getConfig().getString("LanguageFile"));
		if (!ls.getLangFile().isSet(this.getClass().getName()))
			ls.getLangFile().options().copyDefaults(true);
		ls.saveDataLangFile();
		Punishments = new HashMap<String, List<String>>();
		try
		{
			for( String Key : this.getConfig().getConfigurationSection("Punishments").getKeys(false))
			{
				Punishments.put(Key, this.getConfig().getStringList("Punishments." + Key));
			}
			//Bukkit.getLogger().info(Punishments.size()+"");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		/* debug
		for (String str: Punishments.keySet())
		{
			Bukkit.getLogger().info(str);
			Bukkit.getLogger().info(Punishments.get(str).size() + "");
		}*/
		loadData();
		tu = ((Towny) Bukkit.getPluginManager().getPlugin("Towny")).getTownyUniverse();

		BukkitRunnable upkeepAdd = new UpkeepAddRunnable(this);
		upkeepAdd.runTaskTimerAsynchronously(this, 400, (20 * 60/*Seconds*/ * 60 /*Minutes*/ * 1/*Hours*/));
		upkeepRemove = new UpkeepRemoveRunnable(this);
		upkeepRemove.runTaskTimer(this, 800, (20 * 60/*Seconds*/ * 60 /*Minutes*/ * 1/*Hours*/));
		BukkitRunnable upkeepEffect = new UpkeepEffectRunnable(this);
		upkeepEffect.runTaskTimer(this, 800, (20 * this.getConfig().getInt("Punishment.Interval")));

		ListenerBlockBreakEvent lbbe = new ListenerBlockBreakEvent(this, tu);
		ListenerSignChangeEvent lsce = new ListenerSignChangeEvent(this, tu);

		this.getServer().getPluginManager().registerEvents(lbbe, this);
		this.getServer().getPluginManager().registerEvents(lsce, this);
		
		//Run time Command Binding to allow changes for lang

	}


	public void onDisable()
	{
		saveData();
	}

	@SuppressWarnings("deprecation")
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
		if (args.length == 0)
		{
			sendHelp(sender);
			return true;
		}
		String subcommand = args[0];
		if (!sender.hasPermission("TownyFoodUpkeep.tfu"))
		{
			sender.sendMessage(this.getClass().getName() + ".Commands.Main.Subcommand.Main.Deny");
			return true;
		}
		//Bukkit.getLogger().info(subcommand);
		//Bukkit.getLogger().info(ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.List.Name"));
		if (subcommand.equalsIgnoreCase(ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.List.Name")))
		{
			if (!sender.hasPermission("TownyFoodUpkeep.tfu.List"))
			{
				sender.sendMessage(this.getClass().getName() + ".Commands.Main.Subcommand.List.Deny");
				return true;
			}
			for(TownUpkeep tu : TownUpkeepList.values()) 
			{
				if (tu.isOverDue())
				{
					String Return = ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.List.OverDue");
					Return = Return.replace("%t", tu.getTown().getFormattedName());
					Return = Return.replace("%u", tu.getOverDueAmount() + "" );
					Return = Return.replace("%n", tu.getTotalAmount()+"");
					sender.sendMessage(Return);
				}else
				{
					String Return = ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.List.NotOverDue");
					Return = Return.replace("%t", tu.getTown().getFormattedName());
					Return = Return.replace("%n", tu.getTotalAmount() + "" );
					sender.sendMessage(Return);
				}
			}
			return true;
		}
		else if (subcommand.equalsIgnoreCase(ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.Needed.Name")))
		{
			if ( !sender.hasPermission("TownyFoodUpkeep.tfu.Needed") || !(sender instanceof Player) )
			{
				sender.sendMessage(this.getClass().getName() + ".Commands.Main.Subcommand.Needed.Deny");
				return true;
			}
			try
			{
				Player p = (Player) sender;
				Resident r;

				r = tu.getResident(p.getName());
				if(r.hasTown() && TownUpkeepList.containsKey(r.getTown()))
				{
					p.sendMessage(ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.Needed.Header"));
					for(Upkeep uk : TownUpkeepList.get(r.getTown()).getUpkeeps())
					{						
						if(uk.isoverdue())
						{
							String Return = ls.getLangFile().getString(this.getClass().getName()+ ".Commands.Main.Subcommands.Needed.Overdue");
							Return = Return.replace("%m", uk.getMaterial().toString());
							Return = Return.replace("%u", uk.amountoverdue()+"");
							Return = Return.replace("%n", uk.amount()+"");
							p.sendMessage(Return);
						}
						else
						{
							String Return = ls.getLangFile().getString(this.getClass().getName()+ ".Commands.Main.Subcommands.Needed.NotOverdue");
							Return = Return.replace("%m", uk.getMaterial().toString());
							Return = Return.replace("%u", uk.amountoverdue()+"");
							Return = Return.replace("%n", uk.amount()+"");
							p.sendMessage(Return);
						}
					}
				}
				else 
				{
					p.sendMessage("You do not belong to a town with upkeep");
				}
			}
			catch (Exception e)
			{
				//Should never happen, Checks are performed before calling.
			}
			return true;
		}
		else if (subcommand.equalsIgnoreCase(ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.Chest.Name")))
		{
			if(!sender.hasPermission("TownyFoodUpkeep.tfu.Chest"))
			{
				sender.sendMessage(this.getClass().getName() + ".Commands.Main.Subcommand.Chest.Deny");
				return true;
			}
			sender.sendMessage(ls.getLangFile().getString(this.getClass().getName()+ ".Commands.Main.Subcommands.Chest.Header"));
			for (TownChest tc : TownChestList.values())
			{
				String TReturn = ls.getLangFile().getString(this.getClass().getName()+ ".Commands.Main.Subcommands.Chest.Town");
				TReturn = TReturn.replace("%t", tc.getTown().getFormattedName());
				sender.sendMessage(TReturn);
				for( Location loc : tc.chestlist)
				{
					String Return = ls.getLangFile().getString(this.getClass().getName()+ ".Commands.Main.Subcommands.Chest.Chest");
					Return = Return.replace("%t", tc.getTown().getFormattedName());
					Return = Return.replace("%x", loc.getBlockX()+"");
					Return = Return.replace("%y", loc.getBlockY()+"");
					Return = Return.replace("%z", loc.getBlockZ()+"");
					sender.sendMessage(Return);

				}
			}
			return true;
		}
		else if (subcommand.equalsIgnoreCase(ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.TakeItems.Name")))
		{
			if(!sender.hasPermission("TownyFoodUpkeep.tfu.takeitem"))
			{
				sender.sendMessage(this.getClass().getName() + ".Commands.Main.Subcommand.TakeItems.Deny");
				return true;
			}
			upkeepRemove.run();		
			return true;
		}
		else if (subcommand.equalsIgnoreCase(ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.RunAgain.Name")))
		{
			if(!sender.hasPermission("TownyFoodUpkeep.tfu.runagain"))
			{
				sender.sendMessage(this.getClass().getName() + ".Commands.Main.Subcommand.RunAgain.Deny");
				return true;
			}
			UpkeepAddRunnable AUR = new UpkeepAddRunnable(this, true);
			AUR.runTaskAsynchronously(this);
			return true;		
			
		}		
		else if (subcommand.equalsIgnoreCase(ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.AdminNeeded.Name")))
		{
			if(!sender.hasPermission("TownyFoodUpkeep.tfu.adminneeded"))
			{
				sender.sendMessage(this.getClass().getName() + ".Commands.Main.Subcommand.AdminNeeded.Deny");
				return true;
			}
			try
			{
				for(Upkeep uk : TownUpkeepList.get(tu.getTown(args[0])).getUpkeeps())
				{						
						if(uk.isoverdue())
						{
							String Return = ls.getLangFile().getString(this.getClass().getName()+ ".Commands.Main.Subcommands.Needed.Overdue");
							Return = Return.replace("%m", uk.getMaterial().toString());
							Return = Return.replace("%u", uk.amountoverdue()+"");
							Return = Return.replace("%n", uk.amount()+"");
							sender.sendMessage(Return);
						}
						else
						{
							String Return = ls.getLangFile().getString(this.getClass().getName()+ ".Commands.Main.Subcommands.Needed.NotOverdue");
							Return = Return.replace("%m", uk.getMaterial().toString());
							Return = Return.replace("%u", uk.amountoverdue()+"");
							Return = Return.replace("%n", uk.amount()+"");
							sender.sendMessage(Return);
						}
					}
			}
			catch (Exception e)
			{
				sender.sendMessage(this.getClass().getName() + ".Commands.Main.Subcommand.AdminNeeded.NotExist");
			}
			return true;
			
		}		
		else
		{
			sendHelp(sender);
			return true;
		}

	}

	private void sendHelp(CommandSender sender) 
	{
		List<String> Help = ls.getLangFile().getStringList(this.getClass().getName() + ".Commands.Main.Help");
		for(String str : Help)
		{
			String Return = str;
			Return = Return.replace("%c", ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Name"));
			Return = Return.replace("%1", ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.List.Name"));
			Return = Return.replace("%2", ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.Needed.Name"));
			Return = Return.replace("%3", ls.getLangFile().getString(this.getClass().getName() + ".Commands.Main.Subcommands.Chest.Name"));
			sender.sendMessage(Return);
		}
	}


	public void saveData() 
	{
		try
		{
		List<String> TownUpkeepList = new ArrayList<String>();
		List<String> TownChestList = new ArrayList<String>();
		for ( TownUpkeep tu : this.TownUpkeepList.values())
			TownUpkeepList.add(tu.toSerialString());
		for ( TownChest tc : this.TownChestList.values())
			TownChestList.add(tc.toSerialString());
		ds.getDataStorage().set("TownUpkeep", TownUpkeepList);
		ds.getDataStorage().set("TownChestList", TownChestList);
		ds.saveDataStorage();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		

	}
	public void loadData()
	{

		List<String> TownUpkeepStringList = new ArrayList<String>();
		List<String> TownChestStringList = new ArrayList<String>();

		TownyUniverse townyuniverse = ((Towny) Bukkit.getPluginManager().getPlugin("Towny")).getTownyUniverse();
		try
		{
			TownUpkeepStringList = ds.getDataStorage().getStringList("TownUpkeep");
			TownChestStringList = ds.getDataStorage().getStringList("TownChestList");
		}
		catch (Exception e)
		{

		}
		TownUpkeepList = new IdentityHashMap<Town, TownUpkeep>();
		TownChestList = new IdentityHashMap<Town, TownChest>();		
		for(String tu : TownUpkeepStringList)
		{
			try 
			{
				TownUpkeep newtu = new TownUpkeep(tu, townyuniverse);
				TownUpkeepList.put(newtu.getTown(), newtu);
			} 
			catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		for(String tc : TownChestStringList)
		{
			try 
			{
				TownChest newtc = new TownChest(tc, townyuniverse);
				TownChestList.put(newtc.getTown(), newtc);
			}
			catch (NotRegisteredException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


	}
	public boolean addupkeep(Material material , int amount, Town town)
	{
		if (TownUpkeepList.containsKey(town))
		{
			TownUpkeepList.get(town).addUpkeep(material, amount);				
			return true;
		}
		else
		{
			TownUpkeep newtu = new TownUpkeep(town);
			newtu.addUpkeep(material, amount);		
			TownUpkeepList.put(town, newtu);
			return true;
		}
	}
	public boolean isOverDue(Town town)
	{
		if (TownUpkeepList.containsKey(town))
			return TownUpkeepList.get(town).isOverDue();
		return false;
	}
	public int amountOverDue(Town town)
	{
		int amount = 0;
		for (Upkeep uk : TownUpkeepList.get(town).getOverDue())
		{
			amount += uk.amountoverdue();
		}
		return amount;
	}
	public boolean addTownChest(Town town, Location loc)
	{
		if (TownChestList.containsKey(town))
		{
			TownChestList.get(town).addchest(loc);
			return true;
		}
		else
		{
			TownChest newtc = new TownChest(town);
			newtc.addchest(loc);
			TownChestList.put(newtc.getTown(),  newtc);
			return true;
		}
	}
	public boolean removeTownChest(Location loc)
	{
		for ( TownChest tc : TownChestList.values())
		{
			tc.removechest(loc);
		}
		return true;
	}
	public TownUpkeep getTownupkeep(Town town)
	{
		if (TownUpkeepList.containsKey(town))
		{
			return TownUpkeepList.get(town);
		}
		return null;
	}
	public boolean isNumeric(String s) {  
		return s.matches("[-+]?\\d*\\.?\\d+");  
	}


	public boolean isWarehouse(Location location) {
		for (TownChest tc : TownChestList.values())
			if (tc.isTownChest(location))
				return true;
		return false;
	}  
}