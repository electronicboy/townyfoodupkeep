package net.potatohed.TownyFoodUpkeep;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class DataStorage 
{
	private FileConfiguration customConfig = null;
	private File customConfigFile = null;
	private TownyFoodUpkeep main;
	public DataStorage(Plugin plugin)
	{
		main = (TownyFoodUpkeep) plugin;
	}
	@SuppressWarnings("deprecation")
	public void reloadDataStorage() 
	{
		if (customConfigFile == null) {
			customConfigFile = new File(main.getDataFolder(), "DataStorage.yml");
		}
		customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		// Look for defaults in the jar
		InputStream defConfigStream = main.getResource("DataStorage.yml");
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			customConfig.setDefaults(defConfig);
		}
	}
	public FileConfiguration getDataStorage() 
	{
	    if (customConfig == null) {
	    	reloadDataStorage();
	    }
	    return customConfig;
	}
	public void saveDataStorage() {
	    if (customConfig == null || customConfigFile == null) {
	        return;
	    }
	    try {
	        getDataStorage().save(customConfigFile);
	    } catch (IOException ex) {
	        main.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
	    }
	}
	












}
