package net.potatohed.TownyFoodUpkeep;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class LangStrings 
{
	private FileConfiguration customConfig = null;
	private File customConfigFile = null;
	private TownyFoodUpkeep main;
	private String LangFile;
	public LangStrings(Plugin plugin, String LangFile)
	{
		main = (TownyFoodUpkeep) plugin;
		this.LangFile = LangFile;
	}
	@SuppressWarnings("deprecation")
	public void reloadLangFile() 
	{
		if (customConfigFile == null) {
			customConfigFile = new File(main.getDataFolder(), LangFile);
		}
		customConfig = YamlConfiguration.loadConfiguration(customConfigFile);

		// Look for defaults in the jar
		InputStream defConfigStream = main.getResource(LangFile);
		if (defConfigStream != null) {
			YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			customConfig.setDefaults(defConfig);
		}
	}
	public FileConfiguration getLangFile() 
	{
	    if (customConfig == null) {
	    	reloadLangFile();
	    }
	    return customConfig;
	}
	public void saveDataLangFile() {
	    if (customConfig == null || customConfigFile == null) {
	        return;
	    }
	    try {
	        getLangFile().save(customConfigFile);
	    } catch (IOException ex) {
	        main.getLogger().log(Level.SEVERE, "Could not save config to " + customConfigFile, ex);
	    }
	}
	












}
