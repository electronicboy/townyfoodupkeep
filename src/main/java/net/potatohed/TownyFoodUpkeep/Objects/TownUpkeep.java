package net.potatohed.TownyFoodUpkeep.Objects;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;

import com.palmergames.bukkit.towny.exceptions.NotRegisteredException;
import com.palmergames.bukkit.towny.object.Town;
import com.palmergames.bukkit.towny.object.TownyUniverse;

public class TownUpkeep 
{
	Town town;
	List<Upkeep> Upkeeps;
	int PLevel = -1;

	public TownUpkeep(Town town)
	{
		this.town = town;
		Upkeeps = new ArrayList<Upkeep>();
	}
	@SuppressWarnings("deprecation")
	public TownUpkeep(String serializedStr, TownyUniverse tu) throws NotRegisteredException
	{
		String[] info = serializedStr.split("�");
		town = tu.getTown( info[0] );
		Upkeeps = new ArrayList<Upkeep>();
		for (int i = 1; i < info.length ; i++)
		{
			Upkeeps.add(new Upkeep(info[i]));
		}
	}
	public boolean addUpkeep(Material material, int amount)
	{
		try
		{
			boolean set = false;
			for (Upkeep upkeep : Upkeeps )
			{
				if (upkeep.getMaterial() == material)
				{
					set = true;
					upkeep.add(amount);
					return true;
				}
			}
			if (set == false)
				Upkeeps.add(new Upkeep(material, amount));
			return true;

		} catch (Exception e)
		{
			return false;
		}

	}
	public boolean removeUpkeep(Material material, int amount)
	{
		try
		{
			for (Upkeep upkeep : Upkeeps )
			{
				if (upkeep.getMaterial() == material)
				{
					upkeep.remove(amount);
					return true;
				}
			}
			return false;

		} catch (Exception e)
		{
			return false;
		}

	}
	public List<Upkeep> getOverDue()
	{
		List<Upkeep> returnUpkeep = new ArrayList<Upkeep>(); 
		for ( Upkeep upkeep : Upkeeps)
		{
			if (upkeep.isoverdue())
			{
				returnUpkeep.add(upkeep);
			}
		}
		return returnUpkeep;
	}
	public int getOverDueAmount()
	{
		int returnamount = 0;
		for ( Upkeep upkeep : Upkeeps)
		{
			if (upkeep.isoverdue())
			{
				returnamount += upkeep.amountoverdue();
			}
		}
		return returnamount;
	}
	public int getTotalAmount()
	{
		int returnamount = 0;
		for ( Upkeep upkeep : Upkeeps)
		{
				returnamount += upkeep.amount();
		}
		return returnamount;
	}
	public void clearUpkeep()
	{
		Upkeeps = new ArrayList<Upkeep>();
	}
	public boolean isOverDue()
	{
		for (Upkeep upkeep : Upkeeps)
		{
			if (upkeep.isoverdue())
				return true;
		}
		return false;
	}
	public String toSerialString()
	{
		String returnstring = town.getName();
		for (Upkeep upkeep : Upkeeps)
			returnstring += "�" + upkeep.toSerialString();
		return  returnstring;
	}
	public Town getTown()
	{
		return town;
	}
	public List<Upkeep> getUpkeeps()
	{
		return Upkeeps;
	}
	public int amountNeeded(Material mat)
	{
		for( Upkeep uk : Upkeeps)
		{
			if (uk.getMaterial() == mat)
				return uk.amount();
		}
		return 0;
	}
	public boolean resetLAmount()
	{
		for(Upkeep uk : Upkeeps)
			uk.resetLAmount();
		return true;
	}
}
